# Build Directions

## EASY METHOD

- Install [nix][nix]
- Run `nix-shell` in the XeOS folder
- Run `cargo xbuild` in the XeOS folder

[nix]: https://nixos.org/nix/

## Less easy method

```
$ rustup install nightly-2020-04-08
$ rustup default nightly-2020-04-08
$ cargo install cargo-xbuild
$ cargo install bootloader
$ cargo xbuild
```

Other versions of Rust might work, but the version listed in
[`rust.nix`][rustnix] is the most accurate.

[rustnix]: https://tulpa.dev/cadey/XeOS/src/branch/master/nix/rust.nix
