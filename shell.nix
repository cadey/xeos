let
  pkgs = import <nixpkgs> { };
  sources = import ./nix/sources.nix;
  rust = import ./nix/rust.nix { inherit sources; };
  bootimage = import ./nix/bootimage.nix { inherit sources pkgs; };
in pkgs.mkShell {
  buildInputs = with pkgs; [ rust cargo-xbuild qemu bootimage ];
}
