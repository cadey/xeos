{ sources ? import ./sources.nix, rust ? import ./rust.nix { }
, pkgs ? import sources.nixpkgs { } }:

with pkgs;

rustPlatform.buildRustPackage rec {
  pname = "bootimage";
  version = "0.7.9";

  inherit rust;

  src = fetchFromGitHub {
    owner = "rust-osdev";
    repo = pname;
    rev = "v${version}";
    sha256 = "03pqia1gi2i1dacnxmc2ryspdim2a8dinhipagnbb402q58qbvw9";
  };

  cargoSha256 = "0lasshl4iwv6lrc65588rd4h4mh8h2cv1qfdnqy7mhrbmxrzxvyc";
}

