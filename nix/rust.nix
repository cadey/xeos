{ sources ? import ./sources.nix }:

let
  pkgs =
    import sources.nixpkgs { overlays = [ (import sources.nixpkgs-mozilla) ]; };
in (pkgs.rustChannelOf {
  date = "2020-04-08";
  channel = "nightly";
}).rust.override {
  extensions = [
    "rust-src"
    "rust-analysis"
    "rls-preview"
    "rustfmt-preview"
  ];
}
