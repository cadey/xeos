#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(xe_os::test_runner)]
#![reexport_test_harness_main = "test_main"]

use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;
use xe_os::{println, serial_print, serial_println};

entry_point!(kmain);

fn kmain(_: &'static BootInfo) -> ! {
    test_main();

    loop {}
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    xe_os::test_panic_handler(info);
}

#[test_case]
fn test_println() {
    serial_print!("test_println... ");
    println!("test_println output");
    serial_println!("[ok]");
}
