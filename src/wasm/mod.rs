use crate::{print, println};
use alloc::vec::Vec;
use wasmi::{
    Error as InterpreterError, Externals, FuncInstance, FuncRef, ImportsBuilder,
    ModuleImportResolver, ModuleInstance, RuntimeArgs, RuntimeValue, Signature, Trap, ValueType,
};

#[cfg(test)]
use crate::{serial_print, serial_println};

mod hack;

async fn load_wasm_module() -> wasmi::Module {
    let bytes = include_bytes!("../../data/h.wasm");
    let b: Vec<u8> = bytes.iter().cloned().collect();
    println!("    loaded wasm into a Vec");

    wasmi::Module::from_buffer(&b).unwrap()
}

pub async fn run() {
    let module = load_wasm_module().await;
    let mut imports = ImportsBuilder::new();
    imports.push_resolver("h", &H);

    let instance = ModuleInstance::new(&module, &imports)
        .expect("failed to load wasm module")
        .assert_no_start();

    println!("    invoking export");
    let _ = instance.invoke_export("h", &[], &mut H);
}

#[test_case]
fn test_wasm() {
    serial_print!("running h example... ");

    let _ = run();
    serial_println!("[ok]");
}

struct H;
const H_FUNC: usize = 0;

impl Externals for H {
    fn invoke_index(
        &mut self,
        index: usize,
        args: RuntimeArgs,
    ) -> Result<Option<RuntimeValue>, Trap> {
        use core::char::from_u32;
        match index {
            H_FUNC => {
                let ch_t: i32 = args.nth(0);
                let ch = from_u32(ch_t as u32);
                print!("{}", ch.unwrap());
                Ok(None)
            }

            _ => panic!("unknown function called"),
        }
    }
}

impl ModuleImportResolver for H {
    fn resolve_func(
        &self,
        field_name: &str,
        _signature: &Signature,
    ) -> Result<FuncRef, InterpreterError> {
        let func_ref = match field_name {
            "h" => FuncInstance::alloc_host(Signature::new(&[ValueType::I32][..], None), H_FUNC),
            _ => panic!("unknwon function asked for"),
        };

        Ok(func_ref)
    }
}
