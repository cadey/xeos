#[no_mangle]
extern "C" fn fmod(_x: f64, _y: f64) -> f64 {
    unimplemented!();
}

#[no_mangle]
extern "C" fn fmodf(_x: f32, _y: f32) -> f32 {
    unimplemented!();
}

#[no_mangle]
extern "C" fn fmin(_x: f64, _y: f64) -> f64 {
    unimplemented!();
}

#[no_mangle]
extern "C" fn fminf(_x: f32, _y: f32) -> f32 {
    unimplemented!();
}

#[no_mangle]
extern "C" fn fmax(_x: f64, _y: f64) -> f64 {
    unimplemented!();
}

#[no_mangle]
extern "C" fn fmaxf(_x: f32, _y: f32) -> f32 {
    unimplemented!();
}

#[no_mangle]
extern "C" fn __truncdfsf2(_x: f64) -> f32 {
    unimplemented!();
}
