use chrono::prelude::*;
use cmos::{CMOSCenturyHandler, CMOS};
use core::convert::TryInto;
use lazy_static::lazy_static;
use spin::Mutex;

lazy_static! {
    static ref CLOCK: Mutex<CMOS> = Mutex::new(unsafe { CMOS::new() });
}

pub fn current_time() -> DateTime<Utc> {
    let rtc = CLOCK.lock().read_rtc(CMOSCenturyHandler::CurrentYear(2000));
    return Utc
        .ymd(
            rtc.year.try_into().unwrap(),
            rtc.month.into(),
            rtc.day.into(),
        )
        .and_hms(rtc.hour.into(), rtc.minute.into(), rtc.second.into());
}

#[cfg(test)]
use crate::{println, serial_print, serial_println};

#[test_case]
fn test_current_time() {
    serial_print!("current time... ");
    let now = current_time();
    println!("{:?}", now);

    assert_ne!(now.month(), 0);

    serial_println!("[ok]");
}
