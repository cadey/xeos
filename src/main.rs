#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(xe_os::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use alloc::string::ToString;
use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;
use xe_os::{clock, init, println, wasm, task::{Task, executor::Executor, keyboard}};

entry_point!(kernel_main);

fn kernel_main(boot_info: &'static BootInfo) -> ! {
    init(&boot_info);

    #[cfg(test)]
    test_main();

    // Read the rtc date time using this year
    let now = clock::current_time();
    println!("    started at {}", now.to_string());

    let mut executor = Executor::new();
    executor.spawn(Task::new(h_task()));
    executor.spawn(Task::new(example_task()));
    executor.spawn(Task::new(keyboard::print_keypresses()));

    println!("[a] Starting async executor");
    executor.run();
}

// our existing panic handler
#[cfg(not(test))] // new attribute
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    xe_os::hlt_loop();
}

// our panic handler in test mode
#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    xe_os::test_panic_handler(info);
}

async fn async_number() -> u32 {
    42
}

async fn example_task() {
    println!("[n] Starting number task");
    let number = async_number().await;
    println!("    async number: {}", number);
}

async fn h_task() {
    println!("[w] Running WASM");
    wasm::run().await;
    println!("    success");
}
